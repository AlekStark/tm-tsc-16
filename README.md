# TASK MANAGER

SCREEN: https://disk.yandex.ru/d/VFvqzgPGS83FWw?w=1

## DEVELOPER INFO

name: Aleksandr Fedorov-Karitsky

e-mail: afedorovkaritsky@tsconsulting.com

e-mail: AlekStark@live.ru

## HARDWARE

CPU: i5

RAM: 16Gb

## SOFTWARE

System: Windows 10

Version JDK: 15.0.1

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN
```bash
java -jar ./task-manager.jar
```

