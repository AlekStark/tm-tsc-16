package com.tsc.afedorovkaritsky.tm.exception.system;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(final String value) {
        super("Error! This value ''" + value + "'' is not number...");
    }

}

