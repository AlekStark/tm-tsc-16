package com.tsc.afedorovkaritsky.tm.exception.empty;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
