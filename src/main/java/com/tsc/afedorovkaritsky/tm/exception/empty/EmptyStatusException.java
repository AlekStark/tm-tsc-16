package com.tsc.afedorovkaritsky.tm.exception.empty;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}
