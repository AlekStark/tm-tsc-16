package com.tsc.afedorovkaritsky.tm.exception.entity;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project was not found...");
    }

}
