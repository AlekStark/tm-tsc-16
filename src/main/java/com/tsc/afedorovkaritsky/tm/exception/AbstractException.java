package com.tsc.afedorovkaritsky.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }

}
