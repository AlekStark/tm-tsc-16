package com.tsc.afedorovkaritsky.tm.exception.empty;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}
