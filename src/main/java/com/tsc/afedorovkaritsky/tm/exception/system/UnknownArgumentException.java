package com.tsc.afedorovkaritsky.tm.exception.system;

import com.tsc.afedorovkaritsky.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String arg) {
        super("Error! Argument ''" + arg + "'' was not found...");
    }

}

