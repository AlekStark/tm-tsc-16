package com.tsc.afedorovkaritsky.tm.controller;

import com.tsc.afedorovkaritsky.tm.api.controller.ITaskController;
import com.tsc.afedorovkaritsky.tm.api.service.ITaskService;
import com.tsc.afedorovkaritsky.tm.enumerated.Sort;
import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.exception.entity.TaskNotFoundException;
import com.tsc.afedorovkaritsky.tm.model.Task;
import com.tsc.afedorovkaritsky.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[Список задач]");
        System.out.println("[Укажите вид сортировки]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + "." + task.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    public void showTaskById() {
        System.out.println("[Введите ID задачи]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        System.out.println("[Введите название задачи]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    private void showTask(Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id Задачи: " + task.getId());
        System.out.println("Название: " + task.getName());
        System.out.println("Описание: " + task.getDescription());
        System.out.println("Статус: " + task.getStatus());
        if (task.getProjectId() != null)
            System.out.println("Id проекта:" + task.getProjectId());
        else
            System.out.println("Задача не привязана к проекту");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[Создать задачу]");
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[Введите ID задачи]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[Введите название задачи]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[Введите Id]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskById(id, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskById() {
        System.out.println("Введите Id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByName() {
        System.out.println("[Введите название задачаа]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskById() {
        System.out.println("Введите Id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[Введите название задачаа]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusTaskById() {
        System.out.println("Введите Id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите статус");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusTaskById(id, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusTaskByName() {
        System.out.println("[Введите название задачаа]");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите статус");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusTaskByName(name, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Введите статус");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusTaskByIndex(index, status);
        if (task == null) throw new TaskNotFoundException();
    }

}
