package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.service.ILogService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public class LogService implements ILogService {

    private static final String FILE_NAME = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";
    private static final String COMMANDS_FILE = "./commands.log";

    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_FILE = "./messages.log";

    private static final String ERRORS = "ERRORS";
    private static final String ERRORS_FILE = "./errors.log";

    private final ConsoleHandler consoleHandler = getConsoleHandler();
    private final LogManager manager = LogManager.getLogManager();
    private final Logger root = Logger.getLogger("");
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger messages = Logger.getLogger(MESSAGES);
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registery(errors, ERRORS_FILE, true);
        registery(commands, COMMANDS_FILE, false);
        registery(messages, MESSAGES_FILE, true);

    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }


    private void init() {
        try {
            final InputStream InputStream = LogService.class.getResourceAsStream(FILE_NAME);
            manager.readConfiguration(InputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registery(final Logger logger, final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
