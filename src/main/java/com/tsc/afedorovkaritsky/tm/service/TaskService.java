package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.api.service.ITaskService;
import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.exception.empty.*;
import com.tsc.afedorovkaritsky.tm.exception.entity.TaskNotFoundException;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task removeTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index >= taskRepository.getCount()) throw new EmptyIndexException();
        return taskRepository.removeTaskByIndex(index);
    }

    @Override
    public Task removeTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeTaskByName(name);
    }

    @Override
    public Task removeTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeTaskById(id);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.updateTaskById(id, name, description);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index >= taskRepository.getCount()) throw new EmptyIndexException();
        return taskRepository.updateTaskByIndex(index, name, description);
    }

    public Task startTaskById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startTaskById(id);
    }

    public Task startTaskByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startTaskByName(name);
    }

    public Task startTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.startTaskByIndex(index);
    }

    public Task finishTaskById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishTaskById(id);
    }

    public Task finishTaskByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishTaskByName(name);
    }

    public Task finishTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.finishTaskByIndex(index);
    }

    @Override
    public Task changeStatusTaskById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusTaskById(id, status);
    }

    @Override
    public Task changeStatusTaskByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusTaskByName(name, status);
    }

    @Override
    public Task changeStatusTaskByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusTaskByIndex(index, status);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator<Task> taskComparator) {
        if (taskComparator == null)
            return Collections.emptyList();
        return taskRepository.findAll(taskComparator);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskById(id);
    }

    @Override
    public Task findTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findTaskByName(name);
    }

    @Override
    public Task findTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index >= taskRepository.getCount()) throw new EmptyIndexException();
        return taskRepository.findTaskByIndex(index);
    }

}
