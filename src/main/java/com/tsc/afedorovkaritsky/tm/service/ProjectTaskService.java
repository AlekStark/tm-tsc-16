package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.repository.IProjectRepository;
import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectTaskService;
import com.tsc.afedorovkaritsky.tm.exception.empty.EmptyIdException;
import com.tsc.afedorovkaritsky.tm.exception.empty.EmptyIndexException;
import com.tsc.afedorovkaritsky.tm.exception.empty.EmptyNameException;
import com.tsc.afedorovkaritsky.tm.exception.entity.ProjectNotFoundException;
import com.tsc.afedorovkaritsky.tm.exception.entity.TaskNotFoundException;
import com.tsc.afedorovkaritsky.tm.model.Project;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        return taskRepository.findAllTasksByProjectId(projectId);
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProjectById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
        return taskRepository.unbindTaskFromProjectById(projectId, taskId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeTasksByProjectId(projectId);
        return projectRepository.removeProjectById(projectId);
    }

    @Override
    public Project removeProjectByIndex(Integer projectIndex) {
        if (projectIndex == null || projectIndex < 0) throw new EmptyIndexException();
        Project project = projectRepository.findProjectByIndex(projectIndex);
        String projectId = project.getId();
        taskRepository.removeTasksByProjectId(projectId);
        return projectRepository.removeProjectById(projectId);
    }

    @Override
    public Project removeProjectByName(String projectName) {
        if (projectName == null || projectName.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findProjectByName(projectName);
        String projectId = project.getId();
        taskRepository.removeTasksByProjectId(projectId);
        return projectRepository.removeProjectById(projectId);
    }

}
