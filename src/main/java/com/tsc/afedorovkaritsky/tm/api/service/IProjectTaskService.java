package com.tsc.afedorovkaritsky.tm.api.service;

import com.tsc.afedorovkaritsky.tm.model.Project;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String projectId);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskFromProjectById(String projectId, String taskId);

    Project removeProjectById(String projectId);

    Project removeProjectByIndex(Integer projectIndex);

    Project removeProjectByName(String projectName);

}