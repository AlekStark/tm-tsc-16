package com.tsc.afedorovkaritsky.tm.api.repository;

import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> projectComparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findProjectById(final String id);

    Project findProjectByName(final String name);

    Project findProjectByIndex(final Integer index);

    Project removeProjectById(final String id);

    Project removeProjectByName(final String name);

    Project removeProjectByIndex(final Integer index);

    Project updateProjectById(final String id, final String name, final String description);

    Project updateProjectByIndex(final Integer index, final String name, final String description);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project startProjectByIndex(Integer index);

    Project finishProjectById(String id);

    Project finishProjectByName(String name);

    Project finishProjectByIndex(Integer index);

    Project changeStatusProjectById(String id, Status status);

    Project changeStatusProjectByName(String name, Status status);

    Project changeStatusProjectByIndex(Integer index, Status status);

    int getCount();

    boolean existsById(String projectId);

}
