package com.tsc.afedorovkaritsky.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void exit();

    void showArguments();

    void showCommands();

    void showHelp();

    void showInfo();

    void showVersion();

}
