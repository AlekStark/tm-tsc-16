package com.tsc.afedorovkaritsky.tm.api.repository;

import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> taskComparator);

    void clear();

    Task findTaskById(final String id);

    Task findTaskByName(final String name);

    Task findTaskByIndex(final Integer index);

    Task removeTaskById(final String id);

    Task removeTaskByName(final String name);

    Task removeTaskByIndex(final Integer index);

    Task updateTaskById(final String id, final String name, final String description);

    Task updateTaskByIndex(final Integer index, final String name, final String description);

    Task startTaskById(String id);

    Task startTaskByName(String name);

    Task startTaskByIndex(Integer index);

    Task finishTaskById(String id);

    Task finishTaskByName(String name);

    Task finishTaskByIndex(Integer index);

    Task changeStatusTaskById(String id, Status status);

    Task changeStatusTaskByName(String name, Status status);

    Task changeStatusTaskByIndex(Integer index, Status status);

    int getCount();

    List<Task> findAllTasksByProjectId(String projectId);

    boolean existsById(String taskId);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskFromProjectById(String projectId, String taskId);

    void removeTasksByProjectId(String projectId);

}
