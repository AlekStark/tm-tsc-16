package com.tsc.afedorovkaritsky.tm.api.entity;

import com.tsc.afedorovkaritsky.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
