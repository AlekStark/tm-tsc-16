package com.tsc.afedorovkaritsky.tm.api.entity;

public interface IWBS extends IHasName, IHasCreated, IHasStartDate, IHasStatus {
}
