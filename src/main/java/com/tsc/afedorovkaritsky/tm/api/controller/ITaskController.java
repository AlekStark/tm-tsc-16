package com.tsc.afedorovkaritsky.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void showTaskById();

    void showTaskByName();

    void showTaskByIndex();

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByName();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByName();

    void startTaskByIndex();

    void finishTaskById();

    void finishTaskByName();

    void finishTaskByIndex();

    void changeStatusTaskById();

    void changeStatusTaskByName();

    void changeStatusTaskByIndex();

}
