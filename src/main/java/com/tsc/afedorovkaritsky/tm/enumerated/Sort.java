package com.tsc.afedorovkaritsky.tm.enumerated;

import com.tsc.afedorovkaritsky.tm.comparator.ComparatorByCreated;
import com.tsc.afedorovkaritsky.tm.comparator.ComparatorByName;
import com.tsc.afedorovkaritsky.tm.comparator.ComparatorByStartDate;
import com.tsc.afedorovkaritsky.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Сортировка по имени", ComparatorByName.getInstance()),
    CREATED("Сортировка по дате создания", ComparatorByCreated.getInstance()),
    START_DATE("Сортировка по дате начала", ComparatorByStartDate.getInstance()),
    STATUS("Сортировка по статусу", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator){
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
